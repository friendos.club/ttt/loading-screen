"use strict";

const titleField = document.querySelector('#gameinfo .title');
const detailsField = document.querySelector('#gameinfo .gamedetails');
const statusField = document.querySelector('#gameinfo .status');
const fileCountField = document.querySelector('#gameinfo .filecount');
const fileInfoField = document.querySelector('#gameinfo .fileinfo');

const gamemodeMeta = {
  'sandbox': {'name': 'Sandbox', 'color': '#1194f0'},
  'terrortown': {'name': 'Trouble in Terrorist Town', 'color': '#c81919'},
  'murder': {'name': 'Murder', 'color': '#c17695'},
  'darkrp': {'name': 'DarkRP', 'color': '#fe7100'},
};

let g_filesTotal = -1;
let g_filesNeeded = -1;


function updateFileCount() {
  if (g_filesNeeded >= 0) {
    const downloadedTotal = g_filesTotal - g_filesNeeded;

    fileCountField.innerText = `${downloadedTotal} / ${g_filesTotal} files downloaded.`;
  }
}


/*
	Called at the start, when the loading screen finishes loading all assets.

	serverName - Server's name.
		Convar: hostname
		For exmaple: "Garry's Mod Server"
	serverURL - URL for the loading screen.
		Convar: sv_loadingurl
		For example: "http://mywebsite.com/myloadingscreen.html"
	mapName - The name of the map the server is playing.
		For example: "cs_office"
	maxPlayers - Maximum number of players for the server.
		Convar: maxplayers
	steamID64 - 64-bit, numeric Steam community ID of the client joining.
		For example: 76561198012345678
	gamemode - The gamemode the server is currently playing.
		Convar: gamemode
		For example: "deathrun"
	volume - The value of the player's in-game 'snd_musicvolume' console variable (Music Volume), from 0 to 1
	language - The value of the player's in-game 'gmod_language' console variable, a two letter representation of the player's main menu language
*/
function GameDetails(serverName, serverURL, mapName, maxplayers, steamID64, gamemode, volume, language) {
  titleField.innerText = serverName;

  const gn = gamemodeMeta[gamemode]['name'];
  const gc = gamemodeMeta[gamemode]['color'];

  const mc = "#8ab63d";

  detailsField.innerHTML = `playing <span class="gamemode" style="color:${gc};">${gn}</span> on <span class="map" style="color:${mc};">${mapName}</span>`;
}


/*
	Called at the start

	total- Total number of files the client will have to download.
*/
function SetFilesTotal(total) {
  g_filesTotal = parseInt(total);

  updateFileCount();
}


/*
	Called when the client starts downloading a file.

	fileName- The full path and name of the file the client is downloading.
		This path represents the resource's location rather than the actual file's location on the server.
		For example, the file "garrysmod/addons/myAddon/materials/models/bobsModels/car.mdl" will be:
			"materials/models/bobsModels/car.mdl"
*/
function DownloadingFile(fileName) {
  fileInfoField.innerText = fileName;
}


/*
	Called when the client's joining status changes.

	status- Current joining status.
		For example: "Starting Lua..."

	Under normal conditions this would not be fired until game client starts interacting with server files/workshop. This
	means you probably can't use "Retrieving server info" and everything that goes before downloads.
*/
function SetStatusChanged(status) {
  statusField.innerText = status;

  if (!status.toLowerCase().includes("files")) {
    fileInfoField.innerText = "";
    fileCountField.innerText = "";
  }
}


/*
	Called when the number of files remaining for the client to download changes.

	needed- Number of files left for the client to download.
*/
function SetFilesNeeded(needed) {
  g_filesNeeded = parseInt(needed);

  updateFileCount();
}


function TestLoadingScreen() {
  GameDetails(
    "friendos.club TTT | ArcCW / 1-hit Knife / 400+ maps!",
    "http://localhost",
    "ttt_67thway_v9001",
    "18",
    "76561198022102943",
    "terrortown",
    "0.82",
    "en"
  );

  SetStatusChanged("Downloading files...");

  SetFilesTotal("1000");
  SetFilesNeeded("1000");

  let testDownloadInterval = window.setInterval(() => {
    if (g_filesNeeded > 0) {
      SetFilesNeeded(g_filesNeeded - 1);
      DownloadingFile("models/hl2/coolprops/baby.mdl");
    } else {
      clearInterval(testDownloadInterval);
      SetStatusChanged("Starting Lua...");
    }
  }, 3);
}
