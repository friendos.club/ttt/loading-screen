"use strict";

// modified from https://codepen.io/RobotWizard/pen/rRVKVa

(function() {
  console.info('[dvd.js] Initializing');

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  const screenHeight = document.body.clientHeight;
  const screenWidth = document.body.clientWidth;

  const dvd = document.getElementById("dvd");

  const dvdWidth = dvd.width;
  const dvdHeight = dvd.height;

  // start at random x/y pos
  let x = getRandomInt(0, document.body.clientWidth - dvdWidth);
  let y = getRandomInt(0, document.body.clientHeight - dvdHeight);

  console.log(`[dvd.js] Starting at position (${x}, ${y})`)

  // initially move in a random direction
  let dirX = [-1, 1][getRandomInt(0,1)];
  let dirY = [-1, 1][getRandomInt(0,1)];

  const speed = parseFloat("1" + "." + getRandomInt(0,9))

  function animate() {
    if (y + dvdHeight >= screenHeight - 1 || y < 0) {
      dirY *= -1;
    }

    if (x + dvdWidth >= screenWidth - 1 || x < 0) {
      dirX *= -1;
    }

    x += dirX * speed;
    y += dirY * speed;

    dvd.style.left = x + "px";
    dvd.style.top = y + "px";

    window.requestAnimationFrame(animate);
  }

  window.requestAnimationFrame(animate);
})();
